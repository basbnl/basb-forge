<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
// get_header();
?>

  <div id="projectContainer">
		<div class="container">
			<div class="row">
        <div class="col-12 mb-5">
          <h1><?php the_title();?></h1>
        </div>
				<div class="col-sm-3" style="overflow: show">
					<div class="info sticky"  style="">
            <p><strong>Title:</strong> <?php echo get_the_title(); ?></p>
						<?php if(get_field('type')){
							echo '<p><b>Type: </b>';
							the_field('type');
							echo '</p>';
						} ?>
            <?php
            if( have_rows('team') ):
              while ( have_rows('team') ) : the_row();
                if(get_sub_field('solo_project')){
                  echo "<p><b>Solo project</b></p>";
                } elseif (get_sub_field('team')) {
                  echo '<p><b>Team: </b>';
    							the_sub_field('team');
    							echo '</p>';
                }
              endwhile;
            endif;
            ?>
            <br><br>
            <?php
            if( have_rows('related') ):
              echo '<p><b>Related: </b></p>';
              while ( have_rows('related') ) : the_row();
                $project = get_sub_field('project');
                echo '<p><a class="project-link" href="';
                echo get_permalink($project->ID);
                echo '">';
                echo get_the_title($project->ID);
                echo '</a></p>';
              endwhile;
            endif;
            ?>
					</div>

				</div>
				<div id="project" class="col-sm-9">
					<?php while (have_posts()) : the_post(); ?>
            <?php if(get_field('description')){
              echo '<p><strong>';
              echo get_field('description');
              echo '</strong></p>';
            } ?>
							<?php the_content();?>

					<?php endwhile;?>
				</div>
			</div>

		</div>

  </div>

  <script>
  var elements = document.querySelectorAll('.sticky');
  // Stickyfill.add(elements);
  </script>

  <script>
  (function($) {
    $.ajaxSetup({cache:false});
    $page = $('#page');
    $("#projectContainer .project-link").click(function(){
      $('#loader').fadeIn(300);
        var project = $.attr(this, 'href');
        console.log(project);
          $("#cat-container").animate({
            scrollTop: 0
          }, 400, function(){
            $page.removeClass('project');
            setTimeout(function(){
              $("#project-load").load(project, function(){
                  $page.addClass('project');
                  $('#loader').fadeOut(300);
              });
            }, 400);
          });
    return false;
    });
  })( jQuery );

  </script>
