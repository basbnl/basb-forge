jQuery(document).ready(function( $ ) {
  $("#initLoader").fadeOut(1300);
  var $root = $('html, body');
  if($root.hasClass('home')){




  $('a[href^="#"]').click(function() {
    var href = $.attr(this, 'href');

    $root.animate({
        scrollTop: $(href).offset().top
    }, 1800);

    return false;
  });

  var height = $(window).height();
  $module1 = $("#logo .module .cls-1");
  $module2 = $("#logo .module2 .cls-1");

  function scroll(){
    var pos = $(document).scrollTop();

    var shade2 = ((height * 1) - pos) / (height * .8);
    var shade= Math.round((shade2)*100)/100;
    var color = Math.round(255 * shade);
    var negative = 255 - color;


    $("#wrapper-index #content").css('opacity', 1.2-shade);
    $("#text").css('color', 'rgb(' + color + ', ' + color + ', ' + color + ')');
    $("#background").css('background-color', 'rgb(' + negative + ', ' + negative + ', ' + negative + ')');

    if($('#logo').length){
      var shade2 = ((height * 0.85) - pos) / (height * 0.85);
      var shade= Math.round((shade2)*100)/100;
      var color = Math.round(255 * shade);

      $module1.each( function(index){
        $(this).css('transform', 'translateX(' + (pos * (index)) + 'px) rotate(' + - (pos / (index + 2)) + 'deg) scale(' + (1 + (pos / 200)) + ')');
      });

      $module2.each( function(index){
        $(this).css('transform', 'translateY(' + (pos * (index)) + 'px) rotate(' + - (pos / (index + 1)) + 'deg) scale(' + (1 + (pos / 300)) + ')' );
      });

      $("#wrapper-index #background").css('background-color', 'rgba(0,0,0,' + shade + ')');
      $("#wrapper-index #content").css('opacity', shade);
      $("#content").css('box-shadow', '0px 0px 40px -20px rgba(0,0,0,' + 1.1* shade + ')');
      $("#text").css('color', 'rgb(' + color + ', ' + color + ', ' + color + ')');
    }


    var catPos = $('#categories').offset().top + ($('categories').height() / 2);
    var cat = $('#categories');
    if (pos > catPos && !cat.hasClass('fixed')){
      cat.addClass('fixed');
    }
    else if(pos < catPos && cat.hasClass('fixed'))
    {
      cat.removeClass('fixed');
    }
  }

  $(window).scroll(function() {
    if(!$('#page').hasClass('cat')){
      scroll();
    }
  });
  $(window).resize(function() {
    scroll();
  });
  $('.fade-in').addClass('faded');
  scroll();



$.ajax({cache:false});
  // var cat_link = "category/graphic";
  // console.log(cat_link);
  // $("#cat-container").load(cat_link);

  $page = $('#page');
  $(".cat-link").click(function(){
      var cat_link = $.attr(this, 'href');
      console.log(cat_link);
      if($page.hasClass('cat')){
        $("#cat-load").html('');
        $page.removeClass('cat');
        $root.removeClass('cat');
        // window.history.pushState('', '', '');
      }
      else{
        $('#loader').fadeIn(300);
        $("#cat-load").load(cat_link, function(){
          $page.addClass('cat');
          $root.addClass('cat');
            var time = 200;
            $items = $('#items .item');
            $items.each( function(index){
              // $(this).addClass('show');
              var that = this;
              setTimeout( function(){
                $(that).addClass('show');
              }, time);
              time += 200;
            });
            setTimeout( function(){
              $('#loader').fadeOut(300);
            }, 500);

        });
        // window.history.pushState('', '', cat_link);
      }
  return false;
});

$(".page-link").click(function(){
    $('#loader').fadeIn(300);
    var cat_link = $.attr(this, 'href');
    console.log(cat_link);
    if($page.hasClass('cat')){
      $("#cat-load").html('');
      $page.removeClass('cat');
      $root.removeClass('cat');
      // window.history.pushState('', '', '');
    }
    else{
      $("#cat-load").load(cat_link, function(){
        $page.addClass('cat');
        $('#loader').fadeOut(300);
        $root.addClass('cat');
          var time = 200;
          $items = $('#items .item');
          $items.each( function(index){
            // $(this).addClass('show');
            var that = this;
            setTimeout( function(){
              $(that).addClass('show');
            }, time);
            time += 200;
          });
      });
      // window.history.pushState('', '', cat_link);
    }
return false;
});

$('.cross').click(function(){
  $items = $('#items .item');
  $page = $('#page');
  if (!$page.hasClass('project')){

    $items.removeClass('show');
    $page.removeClass('cat');
    $root.removeClass('cat');
    $("#cat-load").html('');
    $root.scrollTop($('#content').offset().top);
  }
  else if($page.hasClass('project')){
    $("#cat-container").animate({
      scrollTop: 0
    }, 400, function(){
      $page.removeClass('project');
    });
  }
  // window.history.pushState('', '', '/');
});



// $.ajaxSetup({cache:false});
// $(".post-link").click(function(){
//     var post_link = $.attr(this, 'href');
//     console.log(post_link);
//     $("#single-post-container").html("content loading");
//     $("#single-post-container").load(post_link);
//
// return false;
// });



}

});
