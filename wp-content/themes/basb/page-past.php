<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */


$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

// get_header( 'dev' );
// wp_head();

?>

<div id="about" class="about">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php the_field('past') ?>
			</div>
		</div>
	</div>
</div>
