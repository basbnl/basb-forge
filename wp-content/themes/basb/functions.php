<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Load functions to secure your WP install.
 */
require get_template_directory() . '/inc/security.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

add_filter('show_admin_bar', '__return_false');


// ajax: generate a list of posts from a list of post types
function my_get_posts()
{
//permission_check(); <-- check nonce and permissions here

// show only published posts
$args = array(
'post_type' => 'post',
'post_status' => 'publish',
'nopaging' => true, // show all posts in one go
);

$posts = get_posts($args);

// put the posts into an array
$arr = array();
foreach ($posts as $post)
{
$entry = array();

// get the post's attributes here
$entry['id'] = $post->ID;
$entry['title'] = $post->post_title;
$arr[] = $entry;

}

// then output in json format
header("Content-Type: application/json");
echo json_encode($arr);

// make sure you have "exit" here
exit;
}

// add into the ajax action chains

// this is for logged in users
add_action('wp_ajax_my_get_posts', 'my_get_posts');

// this is for the rest
add_action('wp_ajax_nopriv_my_get_posts', 'my_get_posts');

add_image_size( 'projects', 500, 500, true );

@ini_set( ‘upload_max_size’ , ’25MB’ );
@ini_set( ‘post_max_size’, ’27MB’);
@ini_set( ‘memory_limit’, ’30MB’ );
