<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-82434127-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-82434127-1');
	</script>


</head>

<body <?php body_class(); ?>>
	<div id="loader">
		<div class="load">
			<img src="<?php echo get_template_directory_uri(); ?>/img/loader.gif"/>
		</div>
	</div>
	<div id="initLoader">
		<div class="load">
			<img src="<?php echo get_template_directory_uri(); ?>/img/loader2.gif"/>
			<h2>BasB.nl</h2>
		</div>
	</div>
<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">
		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<div id="text">
			<h1>Bas Bakx</h1>
			<h2>bas@basb.nl</h2>
			<div class="arrow">
				<i class="fa fa-angle-down up-down" aria-hidden="true"></i>
			</div>
		</div>

	</div><!-- .wrapper-navbar end -->
