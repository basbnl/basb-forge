<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */


$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

// get_header( 'dev' );
// wp_head();

?>

<div id="about" class="about">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php the_field('vision') ?>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<?php $img = get_field('photo'); ?>
				<img src="<?php echo $img['url'] ?>" alt="">
				<figcaption class="wp-caption-text">That's me!</figcaption>
			</div>
		</div>
		<div class="mt-5"></div>
		<div class="row">

			<div class="col-12">
				<?php the_field('identity') ?>
			</div>
		</div>
	</div>
</div>
