<h3>Master</h3>
<table>
	<tbody>
		<tr>
			<td><b>M1.1</b></td>
			<td></td>
			<td></td> 
		</tr>
		<tr>
			<td>BBeacon - Responsive Environments</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Responsive-Environments-BBeacon.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DEM113 - Design Innovation Strategies</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/DEM113-Final-Presentation.pdf">Presentation</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DHM310 - Constructive Design Research</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Poster-DHM310-Bas-Bakx-s123612.pdf">Poster</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Reflection-and-Role-DHM310-Bas-Bakx-s123612.pdf">Reflection</a>
			</td>
		</tr>
		<tr>
			<td><b>M1.2</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Ambient-Mapping - CRIGS</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/CRIGS-Bas-Bakx-M1.2.pdf">Report</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Personal-Reflection-Bas-Bakx-M1.2.pdf">Reflection</a>
			</td>
		</tr>
		<tr>
			<td>DAM110 - Activating your Innovation Radar</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/2016-11-04-ZoutZorg-Final-Paper.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><b>M2.1/M2.2</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Growth</td>
			<td>Report under NDA</td>
			<td></td>
		</tr>
		<tr>
			<td>DDM110 - Design for behavioral change</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Team-5-DDM110.pdf">Report</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Reflection-Bas-Bakx-DDM110.pdf">Reflection</a>
			</td>
		</tr>
	</tbody>
</table>
&nbsp;
&nbsp;
&nbsp;
<h2>Bachelor</h2>
<table>
	<tbody>
		<tr>
			<td><b>B1.1</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Froque - Will-o-the-Whisp Wandering</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/report.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG101 - Cardboard Modeling</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/explorations-with-media-players.jpg" rel="noopener">Poster 2</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/models.jpg" rel="noopener">Poster 2</a>
			</td>
		</tr>
		<tr>
			<td>DG605 - Sound Design</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Reflection-on-Sound-Design-Bas-Bakx.doc">Reflection</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><b>B1.2</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Pablob - Smart Sleep</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Final-Report.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG104 - Panamarenko</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/panamarenko-report-2-.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG308 - Discrete Interaction Design</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Final-report.docx">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><b>B2.1</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Loopende Band - GMIS</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Loopende-Band.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG123 - Making Materials</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/MakingMaterials.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG409 - Design for the Environment</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Final-Reflection-Bas-Bakx-DG409.pdf">Reflection</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><b>B2.2</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Print 'n Press - Crafting Stories</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Crafting-Stories-Bas-Bakx.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG622 - Exploring Intuitive and Aesthetic Interaction</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/dg622-reflection.docx">Reflection</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DG703 - Complexity through Simplicity</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Complexity-trough-Simplicity.pdf">Report</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Reflection-Bas-Bakx-DG703.pdf">Reflection</a>
			</td>
		</tr>
		<tr>
			<td><b>B3.1</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Internship</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Internship-OWOW-Bas-Bakx.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><b>B3.2</b></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>E-Xplore - Design for Creatives</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Report-Design-for-Creatives-Bas-Bakx-v2.pdf">Report</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DAB911 - Exploratory Sketching</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Bas-Bakx.pdf">Reflection</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>DEB913 - Designing Tangible Business Models</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/Final-Report-business-model-generation.pdf">Report</a>
			</td>
			<td>
				<a target="_blank" href="http://localhost:8888/basb_forge/wp-content/uploads/2018/01/DEB913-Course-Reflection-Form.pdf">Reflection</a>
			</td>
		</tr>
	</tbody>
</table>
