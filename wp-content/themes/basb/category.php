<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
// get_header();
// wp_head();
?>

<div class="container-fluid">
  <div class="row" id="items">
    <?php while (have_posts()) : the_post(); ?>
      <div class="col-xl-3 col-lg-4 col-sm-6 item">
        <a class="project-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('projects');?></a>
      </div>
    <?php endwhile;?>

  </div>
</div>
<script>
(function($) {
  $.ajaxSetup({cache:false});
  $page = $('#page');
  $(".project-link").click(function(){
    $('#loader').fadeIn(300);
      var project = $.attr(this, 'href');
      console.log(project);

      if($page.hasClass('project')){
        $("#cat-container").animate({
          scrollTop: 0
        }, 400, function(){
          $page.removeClass('project');
          setTimeout(function(){
            $("#project-load").load(project, function(){
                $page.addClass('project');
                $('#loader').fadeOut(300);
            });
          }, 400);
        });
      }
      else{
        $("#cat-container").animate({
          scrollTop: 0
        }, 400, function(){
          $page.removeClass('project');
          setTimeout(function(){
            $("#project-load").load(project, function(){
                $page.addClass('project');
                $('#loader').fadeOut(300);
            });
          }, 0);
        });
      }

  return false;
  });
})( jQuery );

</script>
