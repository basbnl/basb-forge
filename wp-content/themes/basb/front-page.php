<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

?>

<div id="cat-container">
	<div class="top">
		<a class="cross out"><?php include('img/close.svg'); ?></a>
		<a class="cross up"><?php include('img/up.svg'); ?></a>
	</div>
	<div id="project-load"></div>
	<div id="cat-load">

	</div>

</div>
<div class="wrapper" id="wrapper-index">
<div id="background"></div>
<a href="#content" id="scroll"></a>
	<div id="hero"></div>

	<div class="" id="content" tabindex="-1">
		<div class="container categoriesContainer">
			<div id="categories" class="row categories">
				<div class="spacer"></div>
				<div class="upper">
					<a href="<?php echo get_category_link(get_category_by_slug('digital')->term_id);?>" class="category digital cat-link">
						<div class="icon">
							<div class="tagcontainer">
								<div class="tag">&lt;</div>
								<div class="html">html</div>
								<div class="tag">/&gt;</div>
							</div>
						</div>
						<h3>Digital</h3>
					</a>
					<a href="<?php echo get_category_link(get_category_by_slug('industrial')->term_id);?>" class="category industrial cat-link">

						<div class="icon">
							<?php include('img/gears.svg'); ?>
						</div>
						<h3>Industrial</h3>
					</a>
					<a href="<?php echo get_category_link(get_category_by_slug('more')->term_id);?>" class="category more cat-link">
						<div class="icon plus">+</div>
						<h3>More</h3>
					</a>
				</div>
				<div class="d-none d-sm-flex lower">
					<a href="<?php echo home_url( '/me/' ); ?>" class="industrial page-link">
						<h3>Me</h3>
					</a>
				</div>

				<!-- <div class="lower">
					<a href="<?php echo home_url( '/me/' ); ?>" class="industrial page-link">
						<h3>Me</h3>
					</a>
					<a href="<?php echo home_url( '/past/' ); ?>" class="industrial page-link">
						<h3>Past</h3>
					</a>
					<a href="<?php echo home_url( '/present/' ); ?>" class="industrial page-link">
						<h3>Present</h3>
					</a>
					<a href="<?php echo home_url( '/future/' ); ?>" class="industrial page-link">
						<h3>Future</h3>
					</a>
				</div> -->

			</div>
		</div>
	</div>
</div>


</div><!-- Wrapper end -->


<?php get_footer(); ?>


<script>
	var scene = new THREE.Scene();
	// scene.background = new THREE.Color( 0x000000 );
	var camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 100 );
	camera.setFocalLength ( 50 );
	var renderer = new THREE.WebGLRenderer({alpha:true});
	renderer.setClearColor( 0x000000, 0 );
	// renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	document.getElementById('hero').appendChild( renderer.domElement );

	// instantiate a loader
	var loader = new THREE.JSONLoader();

	var controls = new THREE.OrbitControls( camera );
	controls.enableZoom = false;

	// to disable rotation
	controls.enableRotate = false;

	// to disable pan
	controls.enablePan = false;

	// lights

	// var ambientLight = new THREE.SpotLight( 0xffffff, 15, 0, 2000 );
	// scene.add( ambientLight );

	var spotLight = new THREE.PointLight( 0xffffff, .15 , 0, 1000);
	spotLight.position.set( 0,0, 2 );
	scene.add( spotLight );



	var logoLoader = new THREE.JSONLoader();
	var logo = null;
	// load a resource
	logoLoader.load(
		// resource URL
			'<?php bloginfo('template_directory'); ?>/js/logo5.json',
			// Function when resource is loaded
			function ( geometry, materials ) {
				var material = materials[ 0 ];
				// material.morphTargets = true;
				material.color.setHex( 0x000000 );
				// material.envMap = '<?php bloginfo('template_directory'); ?>/js/kenco_warehouse.jpg';
			logo = new THREE.Mesh( geometry, materials );

			scene.add(logo);

			onWindowResize();
		}

	);

	document.addEventListener('mousemove', onMouseMove, false);
	var mouse = {x: 0, y: 0};
	var pox = 0;
		// Follows the mouse event
	function onMouseMove(event) {

	// Update the mouse variable
	event.preventDefault();
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

 // Make the sphere follow the mouse
  var vector = new THREE.Vector3(mouse.x * 5,  mouse.y * 5, 60);
	vector.unproject( camera );
	var dir = vector.sub( camera.position ).normalize();
	var distance = - camera.position.z / dir.z;
	var pos = camera.position.clone().add( dir.multiplyScalar( distance ) );
	spotLight.position.copy(pos);
	// console.log(mouse.x);
	// spotLight.position.z = Math.abs(60 * mouse.x);
	// Make the sphere follow the mouse
//	mouseMesh.position.set(event.clientX, event.clientY, 0);
};

window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize(){

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
		if(window.innerWidth < window.innerHeight){
			logo.rotation.z = -1.57;
			logo.scale.y = .85;
			logo.scale.x = .85;
		}
		else{

			logo.rotation.z = 0;
			logo.scale.y = 1;
			logo.scale.x = 1;
		}

}

var animate = function () {
	requestAnimationFrame( animate );

	var timer = Date.now() * 0.0003;

	if(logo !== null){
		// logo.rotation.y = Math.cos( timer ) * .5;
	}

	spotLight.position.z = 60;

	controls.update();
	camera.position.x = Math.cos( timer ) * 5;
	camera.position.x = Math.cos( timer ) * 5;
	camera.position.z = 20;
	camera.position.y = - jQuery(document).scrollTop()/60;
	camera.lookAt( scene.position );
	renderer.render(scene, camera);
	if(mouse.x == 0 && mouse.y == 0){
		spotLight.position.x = Math.cos( timer*2 ) * 20;
		spotLight.position.y = Math.sin( timer*3 ) * 40;
	}

};
// jQuery( window ).scroll(function() {
//   camera.position.y = - jQuery(document).scrollTop()/60;
// });

animate();
</script>
