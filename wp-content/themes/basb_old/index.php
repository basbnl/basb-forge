<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BasB
 */

get_header(); ?>


<script type="text/javascript">
function generateMailto(lhs,rhs) {
	document.write(lhs + "@" + rhs);
}
</script>
<?php
	$graphic = 6;
	$web = 10;
	$industrial = 7;
	$year = array(14, 8, 11);
?>
<?php
	if (!function_exists('proList')) {
	function proList() {
		echo"<li><a class='port' href='";
		the_permalink();
		echo" 	'><p>";
		the_title();
		echo"</p></a>";
		the_post_thumbnail(500,500);
		echo "</li>";
	}}
?>

<div id="primary" class="content-area">



    <main id="main" class="site-main" role="main">
            <a class="page1" href="#page2"></a>
            <div id="page1">
                <div class="info">
                    <div class="text name">
                        <span class="site">Bas Bakx</span><br>
						<script language="JavaScript" type="text/javascript">generateMailto('Bas', 'BasB.nl')</script>
                    </div>
                    <div class="text design">
                        Graphic<br>
                        Web<br>
                        Industrial<br><br>
						<img class="portlogo" src="img/logo2.png"/>
                    </div>
                </div>

                <div class="portfolio">
                    <p>Portfolio</p>
                    <img class="arrow" src="img/arrow.png"/>
                </div>
            </div>

            <div id="page2">

				<div id="graphic" class="graphic column">
					<h2>Graphic</h2>
					<img src="img/graphic.svg" />
				</div>
				<div class="sub subgraphic" >
					<div class="list">

						<p>
							I love graphic design! For years I have studied the craft of making stuff look nice and thought about what makes them nice. My main focus is combining the digital aspect of Graphic design with the physical aspect of Industrial Design.
							<br><br>
							Below are some examples of illustration, layout and industrial projects I worked on:
						</p>

						<ul>
							<?php foreach ($year as $yeararray){
								query_posts(array('category__and'=>array($graphic, $yeararray)));
								if ( have_posts() ): echo "<li class='year'><p>"; echo get_cat_name($yeararray); echo "</p></li>"; endif;
								if ( have_posts() ) : while ( have_posts() ) : the_post();
								proList();
							endwhile; endif;}?>
						</ul>

					</div>
				</div>

                <div id="web" class="web column">
                    <h2>Web</h2>
                    <img src="img/web.svg" />
                </div>
                <div class="sub subweb">
					<div class="list">
						<p>
						Web Design is a big outlet for me.
						It's a quick way to apply ideas and the dynamics of the internet allows for a lot of iterating and creativity. By distributing my knowledge across web design, web development and web administration I have been able to work on many aspects of the design process involved in making a site.
						<br><br>
						Below are some of the websites I worked on:
						</p>
						<ul>
							<?php foreach ($year as $yeararray){
								query_posts(array('category__and'=>array($web, $yeararray)));
								if ( have_posts() ): echo "<li class='year'><p>"; echo get_cat_name($yeararray); echo "</p></li>"; endif;
								if ( have_posts() ) : while ( have_posts() ) : the_post();
								proList();
							endwhile; endif;}?>
						</ul>
					</div>
                </div>

                <div id='industrial' class="industrial column">
                    <h2>Industrial</h2>
                    <img src="img/industrial.svg" />
                </div>
                <div class="sub subindustrial">
					<div class="list">
						<p>
							Since 2012 I have been studying Industrial Design at the the University of Technology of Eindhoven.
							Currently I am in my final Bachelor semester, working on graduating.
							Industrial Design gives me a lot of freedom to explore and combine different aspects of design and try to figure out what Good Design is.
							<br><br>
							I have developed a personal vision on Design and compiled my Identity related to this. Read them by clicking the link below!<br><br>
							<a class="port industrial link" href="/vision-and-identity/">Vision and Identity</a><br><br>
							Industrial Design at the TU/e uses a competence framework click the following link to see my learning goals and development within this system.
							<br><br><a class="port industrial link" href="/development/">Development</a><br><br>
							You can read about all my previous Industrial Design project by clicking the links below.
							<br><br>

						</p>
						<ul>
							<?php foreach ($year as $yeararray){
								query_posts(array('category__and'=>array($industrial, $yeararray)));
								if ( have_posts() ): echo "<li class='year'><p>"; echo get_cat_name($yeararray); echo "</p></li>"; endif;
								if ( have_posts() ) : while ( have_posts() ) : the_post();
								proList();
							endwhile; endif;}?>
						</ul>
					</div>
                </div>
            </div>

		</main><!-- #main 	-->
	</div><!-- #primary -->

<?php get_footer(); ?>
