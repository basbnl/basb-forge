(function($) {


    $( document ).ready(function() {

       $('.port').magnificPopup({
		   type:'ajax',
		   closeOnContentClick: true,
		   closeOnBgClick: false
	   });

        $(".list").mCustomScrollbar({
            theme:"dark",
            scrollButtons:{ enable: true }
        });

      $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 500);
                    return false;
                }
            }
        });
        // Cache the div so that the browser doesn't have to find it every time the window is resized.
        var text = $('#page1 .text');
        var info = $('#page1 .info');

        // Run the following when the window is resized, and also trigger it once to begin with.
        $(window).resize(function () {
            // Get the current height of the div and save it as a variable.
            var height = info.height();
            // Set the font-size and line-height of the text within the div according to the current height.
            text.css({
                'font-size': ((height / 6)- height/30) + 'px',
                'line-height': height/6 + 'px',
                'margin': -1.5 + '% ' + 0
            })
        }).trigger('resize');

        $('#page2 .column').click(function(){
            var myId = $(this).attr("id");

            if($(this).hasClass('active')){
                $('#page2 .column').removeClass('active');
                $('.sub' + myId).removeClass('active');
				//$('html, body').css({
					//'overflow': 'auto',
					//'height': 'auto'
				//});
            }
            else{
                $('#page2 .column').removeClass('active');
                $(this).addClass('active');

                $('.sub').removeClass('active');
                $('.sub' + myId).addClass('active');
				//$('html, body').css({
					//'overflow': 'hidden',
					//'height': '100%'
				//})
                $('html,body').animate({
                        scrollTop: $('#page2').offset().top
                    }, 500);
            }

        });

        function pulse(){
            $('#page1 .arrow').animate({top: "10px"}, 1000, function(){
                $('#page1 .arrow').animate({top: "0px"}, 1000, function (){pulse()});
            });
        }
        pulse();



    });
})( jQuery );