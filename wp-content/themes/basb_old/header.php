<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package BasB
 */

?><!DOCTYPE html>
<html  <?php language_attributes(); ?>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,600' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!--<script type='text/javascript' src='http://www.basb.dev/wp-content/themes/basb/js/main.js'></script>-->
	<script type='text/javascript' src='/wp-content/themes/basb/js/main.js'></script>

    <script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="/wp-content/themes/basb/css/style2.css">
    <script src="js/jquery.magnific-popup.min.js"></script>
	
	
</head>

<body <?php body_class(); ?>>

	<div id="content" class="site-content">
        <script>

        </script>